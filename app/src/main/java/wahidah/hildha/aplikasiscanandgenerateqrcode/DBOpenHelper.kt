package wahidah.hildha.aplikasiscanandgenerateqrcode


import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context) : SQLiteOpenHelper(context, DB_Name, null, DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMhs =
            "create table mhs(nim text primary key, nama text not null, prodi text not null)"
        db?.execSQL(tMhs)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object {
        val DB_Name = "App07Mahasiswa"
        val DB_Ver = 1
    }
}