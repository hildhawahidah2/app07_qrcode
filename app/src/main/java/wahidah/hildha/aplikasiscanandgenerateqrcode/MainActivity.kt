package wahidah.hildha.aplikasiscanandgenerateqrcode

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    //IntentIntegrator is a part of Zxing-android-embedded library that is used to read QR Code
    lateinit var intentIntegrator: IntentIntegrator
    lateinit var db: SQLiteDatabase
    lateinit var lsAdapter: ListAdapter
    lateinit var builder: AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        intentIntegrator = IntentIntegrator(this)
        builder = AlertDialog.Builder(this)
        btnGenerateQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)
        btnSave.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
        showDataMhs()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnScanQR -> {
                //initiate a barcode scans and plays 'beep' sound when a barcode is detected
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR -> {
                //initiate barcode encoder
                val barCodeEncoder = BarcodeEncoder()
                //encode text in editText into QRCode image using BarcodeEncoder with
                //the size image/bitmap 400x400 pixel
                val bitmap = barCodeEncoder.encodeBitmap(
                    edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE, 400, 400
                )
                imV.setImageBitmap(bitmap)
            }
            R.id.btnSave -> {
                builder.setTitle("Konfirmasi")
                    .setMessage("Apakah Data Yang Dimasukkan Sudah Benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                builder.show()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents != null) {
                edQrCode.setText(intentResult.contents)
                val strToken = StringTokenizer(edQrCode.text.toString(), ";", false)
                edNim.setText(strToken.nextToken())
                edNama.setText(strToken.nextToken())
                edProdi.setText(strToken.nextToken())
            } else {
                Toast.makeText(this, "Dibatalkan", Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    fun showDataMhs() {
        val cursor: Cursor = db.query(
            "mhs", arrayOf("nim as _id", "nama", "prodi"),
            null, null, null, null, "_id asc"
        )
        lsAdapter =
            SimpleCursorAdapter(
                this, R.layout.list_mhs, cursor,
                arrayOf("_id", "nama","prodi"), intArrayOf(R.id.textNim, R.id.textNama, R.id.textProdi),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
            ) as ListAdapter
        lsMhs.adapter = lsAdapter

    }
    fun insertDataMhs(nim: String, nama: String, prodi: String) {
        var cv: ContentValues = ContentValues()
        cv.put("nim", nim)
        cv.put("nama", nama)
        cv.put("prodi", prodi)
        db.insert("mhs", null, cv)
        showDataMhs()

    }


    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMhs(
            edNim.text.toString(),
            edNama.text.toString(),
            edProdi.text.toString()
        )
        edNim.setText("")
        edNama.setText("")
        edProdi.setText("")
        edQrCode.setText("")


    }
}

